import axios from 'axios'

export function getData(cityName){
    return (dispatch)=>{
      return axios
    .get("/api/weather/city?city="+cityName).then(res => {
      dispatch(setData(res.data));
    })
    .catch(err =>
      console.log(err)
    );
}
}

export function setData(data){
    return {
        type: "SET_DATA",
        data : data,
        loading : true,


    }
}