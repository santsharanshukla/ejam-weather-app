import {createStore, applyMiddleware, compose} from 'redux'
import mainReducer from './reducers'
import thunk from "redux-thunk";
const middleware = [thunk];
const store = createStore (
    mainReducer,
    compose(
        applyMiddleware(...middleware),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
      )

)
export default store;