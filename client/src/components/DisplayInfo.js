import React from "react";
import './style.css'
class DisplayInfo extends React.Component {
  render() {
    const data = this.props.data;
    console.log(data)
    return (
      <div>
          <h4> city : {data.location.city}</h4>
          <h4> country : {data.location.country}</h4>
          <h5> temperature : {data.current_observation.condition.temperature}</h5>
          <h5> humidity : {data.current_observation.atmosphere.humidity}</h5>
          <h5>  pressure : {data.current_observation.atmosphere.pressure}</h5>
          <h5>  visibility : {data.current_observation.atmosphere.visibility}</h5>
      </div>
    );
  }
}

export default DisplayInfo;
