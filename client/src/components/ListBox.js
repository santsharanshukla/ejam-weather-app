import React, { Component, Fragment } from "react";
import "./style.css";
import * as actionCreator from "../action/index";
import { connect } from "react-redux";
import DisplayInfo from "./DisplayInfo";

class ListBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectCity: ""
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit() {
    this.props.getData(this.state.selectCity);
  }
  render() {
    return (
      <Fragment>
        <div className="app-bar">
          <h1 className="app-bar-title">Weather App</h1>
        </div>
        <section className="app-section container">
          {this.renderList()}
          <div className="top-margin-small">
            {this.props.loading ? <DisplayInfo data={this.props.data} /> : null}
          </div>
        </section>
      </Fragment>
    );
  }

  renderList() {
    return (
      <div className="form-group top-margin-small">
        <label className="selector-label">Select city</label>
        <select
          className="selector form-control"
          onChange={e => {
            this.setState({ selectCity: e.target.value });
          }}
        >
          <option></option>
          <option>newyork</option>
          <option>washington</option>
          <option>boston</option>
        </select>
        <button onClick={this.handleSubmit}>submit</button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

export default connect(mapStateToProps, actionCreator)(ListBox);
