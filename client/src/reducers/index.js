let defaultState={
    data : {},
    loading : false
    }
    
    const mainReducer =(state=defaultState, action)=>{
        if(action.type==="SET_DATA"){
            return {
                ...state,
                data : action.data,
                loading : action.loading
            }
        }
        else {
            return{
                ...state
            }
        }
    }
    
    export default mainReducer;