const express = require("express");
const router = express.Router();
const keys = require("../../config/keys");

var OAuth = require("oauth");
var header = {
  "X-Yahoo-App-Id": keys.yahoo.app_id
};
var request = new OAuth.OAuth(
  null,
  null,
  keys.yahoo.consumer_key,
  keys.yahoo.consumer_secret,
  "1.0",
  null,
  "HMAC-SHA1",
  null,
  header
);

router.get("/city", async (req, res, err) => {
  const city = req.query.city;
  const url = `https://weather-ydn-yql.media.yahoo.com/forecastrss?location=${city},&format=json`;
  await request.get(url, null, null, function(err, data, result) {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(data);
    }
  });
});

router.get("/cities", async (req, res, err) => {
  const citiesdata = [];
  const { cities } = req.body;
  var counter = 0;

  cities.forEach(
    await function(city) {
      const url = `https://weather-ydn-yql.media.yahoo.com/forecastrss?location=${city},&format=json`;

      request.get(url, null, null, function(err, data, result) {
        if (err) {
          console.log(err);
        } else {
          citiesdata.push(data);
        }
        counter++;
        if (counter === cities.length) {
          res.status(200).send(citiesdata);
        }
      });
    }
  );
});

module.exports = router;
