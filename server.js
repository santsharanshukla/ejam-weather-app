const express = require("express");
const bodyParser = require("body-parser");
const weather  = require("./routes/api/weather")
const cors = require('cors')

const app = express();

app.use(cors());

// Bodyparser middleware
//support parsing of application/x-www-form-urlencoded post data
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);
// support parsing of application/json type post data

app.use(bodyParser.json());

// Routes

app.use('/api/weather', weather);

//server response test
app.get("/test", (req, res) => {
  res.status(200)
     .send("hi");
});

// not found
app.use(function(req, res) {
  res.type("text/plain")
     .status(404)
     .send("not found");
});

const port = 5500; 
app.listen(port, () => console.log(`Server up and running on port ${port} !`));
